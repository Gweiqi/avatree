# Install script for directory: E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Libraries/Install/Eigen")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen/src/SparseCore" TYPE FILE FILES
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/AmbiVector.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/CompressedStorage.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/ConservativeSparseSparseProduct.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/MappedSparseMatrix.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseBlock.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseColEtree.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseCwiseBinaryOp.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseCwiseUnaryOp.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseDenseProduct.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseDiagonalProduct.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseDot.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseFuzzy.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseMatrix.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseMatrixBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparsePermutation.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseProduct.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseRedux.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseSelfAdjointView.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseSparseProductWithPruning.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseTranspose.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseTriangularView.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseUtil.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseVector.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/SparseView.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/SparseCore/TriangularSolver.h"
    )
endif()

