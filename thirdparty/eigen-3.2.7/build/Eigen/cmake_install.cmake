# Install script for directory: E:/Libraries/eigen-3.2.7/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Libraries/Install/Eigen")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE FILE FILES
    "E:/Libraries/eigen-3.2.7/Eigen/Array"
    "E:/Libraries/eigen-3.2.7/Eigen/Cholesky"
    "E:/Libraries/eigen-3.2.7/Eigen/CholmodSupport"
    "E:/Libraries/eigen-3.2.7/Eigen/Core"
    "E:/Libraries/eigen-3.2.7/Eigen/Dense"
    "E:/Libraries/eigen-3.2.7/Eigen/Eigen"
    "E:/Libraries/eigen-3.2.7/Eigen/Eigen2Support"
    "E:/Libraries/eigen-3.2.7/Eigen/Eigenvalues"
    "E:/Libraries/eigen-3.2.7/Eigen/Geometry"
    "E:/Libraries/eigen-3.2.7/Eigen/Householder"
    "E:/Libraries/eigen-3.2.7/Eigen/IterativeLinearSolvers"
    "E:/Libraries/eigen-3.2.7/Eigen/Jacobi"
    "E:/Libraries/eigen-3.2.7/Eigen/LU"
    "E:/Libraries/eigen-3.2.7/Eigen/LeastSquares"
    "E:/Libraries/eigen-3.2.7/Eigen/MetisSupport"
    "E:/Libraries/eigen-3.2.7/Eigen/OrderingMethods"
    "E:/Libraries/eigen-3.2.7/Eigen/PaStiXSupport"
    "E:/Libraries/eigen-3.2.7/Eigen/PardisoSupport"
    "E:/Libraries/eigen-3.2.7/Eigen/QR"
    "E:/Libraries/eigen-3.2.7/Eigen/QtAlignedMalloc"
    "E:/Libraries/eigen-3.2.7/Eigen/SPQRSupport"
    "E:/Libraries/eigen-3.2.7/Eigen/SVD"
    "E:/Libraries/eigen-3.2.7/Eigen/Sparse"
    "E:/Libraries/eigen-3.2.7/Eigen/SparseCholesky"
    "E:/Libraries/eigen-3.2.7/Eigen/SparseCore"
    "E:/Libraries/eigen-3.2.7/Eigen/SparseLU"
    "E:/Libraries/eigen-3.2.7/Eigen/SparseQR"
    "E:/Libraries/eigen-3.2.7/Eigen/StdDeque"
    "E:/Libraries/eigen-3.2.7/Eigen/StdList"
    "E:/Libraries/eigen-3.2.7/Eigen/StdVector"
    "E:/Libraries/eigen-3.2.7/Eigen/SuperLUSupport"
    "E:/Libraries/eigen-3.2.7/Eigen/UmfPackSupport"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/Libraries/eigen-3.2.7/build/Eigen/src/cmake_install.cmake")

endif()

