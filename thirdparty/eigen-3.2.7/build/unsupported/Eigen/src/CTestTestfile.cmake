# CMake generated Testfile for 
# Source directory: E:/Libraries/eigen-3.2.7/unsupported/Eigen/src
# Build directory: E:/Libraries/eigen-3.2.7/build/unsupported/Eigen/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("AutoDiff")
subdirs("BVH")
subdirs("Eigenvalues")
subdirs("FFT")
subdirs("IterativeSolvers")
subdirs("KroneckerProduct")
subdirs("LevenbergMarquardt")
subdirs("MatrixFunctions")
subdirs("MoreVectorization")
subdirs("NonLinearOptimization")
subdirs("NumericalDiff")
subdirs("Polynomials")
subdirs("Skyline")
subdirs("SparseExtra")
subdirs("Splines")
